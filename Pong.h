/*!
 * @file      Ball.cpp
 * @brief     The main widget which also represents the main class of our simple Pong game.
 * @copyright Peer Schneider
 */

#ifndef PONG_H
#define PONG_H

#include <QWidget>
#include <QTimer>

#include <memory>

#include "defines.h"

class PongScene;

QT_BEGIN_NAMESPACE
namespace Ui
{
  class Pong;
}
QT_END_NAMESPACE

class Pong : public QWidget
{
  Q_OBJECT

public:
  Pong(QWidget* parent = nullptr);
  ~Pong();

public slots:
  void onStarted();
  void onStop();

private slots:
  void onTick();

private:
  void updateUI();
  void updateRacketPositions();
  void initialiseBallPosition();
  void goal(Players p);

  struct Score
  {
    int p1{};
    int p2{};

    auto scoreStr() const -> QString;
    void clear();
    bool hasWinner() const;
    auto winnerString() const -> QString;
  } _score;

  // The ui and the scene are both created on the heap so we can keep our header slim.
  std::unique_ptr<Ui::Pong> _ui;
  std::unique_ptr<PongScene> _scene;

  bool _active{};  // Whether a game of pong is currently running or not.
  QTimer _gameTimer;  // Controls the game loop.

  double _p1Position{0.5};  // Position of the racket of player one
  double _p2Position{0.5};  // Positoin of the racket of player two
  QPointF _ballPosition{};  // Ball position on the playing field
  QPointF _ballVector{};  // The direction the ball is moving at the current moment.
};
#endif  // PONG_H
