/*!
 * @file      Ball.cpp
 * @brief     The scene hosting the Pong game.
 * @copyright Peer Schneider
 */

#ifndef PONGSCENE_H
#define PONGSCENE_H

#include <QGraphicsScene>
#include "defines.h"
#include <QPointF>
#include <QVector2D>

class Racket;
class Ball;

class PongScene : public QGraphicsScene
{
  Q_OBJECT
public:
  PongScene(QObject* parent = nullptr);
  ~PongScene() override;

  void showBall();
  void hideBall();

  enum class IntersectionResult
  {
    None,  // No Intersection
    LeftRacket,  // Intersection with the left racket
    RightRacket,  // Intersection with the right racket
    Top,  // Intersection with the top border of the playing field
    Bottom,  // Intersection with the bottom border of the playing field
    Left,  // Intersection with the left border of the playing field
    Right  // Intersection with the right border of the playing field
  };

  /**
   * The ball moves from the old to the new position or rather it wants to. We now check whether
   * it will collide with the playing field border or the rackets.
   * @param oldBallPosition
   *        The position of the ball at the previous tick.
   * @param newBallPosition
   *        The position the ball wants to go to during this tick.
   * @return The information whether the ball intersects an other object on its way and if yes the
   *         point of intersection.
   */
  auto moveBall(QPointF oldBallPosition, QPointF newBallPosition) const
      -> std::tuple<IntersectionResult, QPointF>;

  /**
   * @brief Positions the racket of the specifie player vertically.
   * @param player
   *        The player for whoem the racket shall be positioned.
   * @param position
   *        A double between 0.0 and 1.0. 0.0 means at the top and 1.0 means at the bottom.
   */
  void positionRacket(Players player, double position);

  /**
   * @brief Positions the ball inside the playing field.
   * @param pos
   *        The relative position of the ball inside the field. The position values are between
   *        0.0 and 1.0 and have to be converted into the absolute values before usage.
   */
  void positionBall(QPointF pos);

private:
  auto relativeToAbsolutePos(QPointF relPos) const -> QPointF;
  auto absoluteToRelative(QPointF absPos) const -> QPointF;

  QRectF _fieldRect;

  Ball* _ball{};
  Racket* _racketP1{};
  Racket* _racketP2{};
};

#endif // PONGSCENE_H
