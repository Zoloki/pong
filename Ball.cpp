/*!
 * @file      Ball.cpp
 * @brief     Graphics item representing a ball in the Pong game.
 * @copyright Peer Schneider
 */

#include "Ball.h"
#include <QPainter>
#include <QStyleOptionGraphicsItem>

static constexpr double cDiameter{10.0};

Ball::Ball() {}

//----------------------------------------------------------------------------

QRectF Ball::boundingRect() const
{
  // The pen is used to draw the outline of the ball. The diameter of the ball lays exactly in the
  // middle of the outline and so we have to consider half the pen width on each side when
  // computing the bounding rectangle because the bounding rectangle has to include the whole
  // area we are drawing into. If we would not do that we would may would see drawing artifacts.
  double penWidth     = 1;
  double halfPenWidth = penWidth / 2.0;
  double radius       = cDiameter / 2.0;
  return QRectF{-radius - halfPenWidth, -radius - halfPenWidth, cDiameter + penWidth,
                cDiameter + penWidth};
}

//----------------------------------------------------------------------------

void Ball::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  (void)option;
  (void)widget;

  painter->setPen(Qt::black);
  painter->setBrush(Qt::black);

  double radius = cDiameter / 2.0;
  painter->drawEllipse(QRectF{-radius, -radius, cDiameter, cDiameter});
}

//----------------------------------------------------------------------------

double Ball::diameter() const
{
  return cDiameter;
}
