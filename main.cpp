/*!
 * @file      Ball.cpp
 * @brief
 * @details
 * @copyright Peer Schneider
 */

#include "Pong.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  Pong w;
  w.show();
  return a.exec();
}
