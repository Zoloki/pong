/*!
 * @file      Ball.cpp
 * @brief     The main widget which also represents the main class of our simple Pong game.
 * @copyright Peer Schneider
 */

#include "Pong.h"
#include "PongScene.h"
#include "ui_Pong.h"
#include <random>

static constexpr double FPS{120};

Pong::Pong(QWidget* parent)
    : QWidget(parent), _ui{std::make_unique<Ui::Pong>()}, _scene{std::make_unique<PongScene>()}
{
  _ui->setupUi(this);
  // The pong_field is of type PongField object which itself is derived from QGraphicsView.
  // It is the only view show our scene.
  _ui->pong_field->setScene(_scene.get());
  // No winner yet, hide the according text.
  _ui->winner->hide();
  // Bring the score object into a defined state as well.
  _score.clear();

  // The game timer fires n times per second each time advancing our game a bit. By increasing the
  // FPS the game runs smoother, reducing the FPS it becomes more and more jerking.
  // The game timer is active all the time even if no Pong game is active. That way we can also
  // move the rackets while none active.
  _gameTimer.setInterval(static_cast<int>(1000. / FPS));
  _gameTimer.start();  
  connect(&_gameTimer, &QTimer::timeout, this, &Pong::onTick);

  // Synchronize widget UI with the Pong state.
  updateUI();

  // Put the player rackets into their initial positions.
  _scene->positionRacket(Players::P1, _p1Position);
  _scene->positionRacket(Players::P2, _p2Position);
}

//----------------------------------------------------------------------------

Pong::~Pong()
{
  _ui->pong_field->setScene(nullptr);
}

//----------------------------------------------------------------------------

void Pong::onStarted()
{
  // The start button got pressed, initialize the game and start it by 'activating' the ball.
  _score.clear();
  _ui->winner->hide();
  initialiseBallPosition();
  _scene->showBall();
  _active = true;
  updateUI();
}

//----------------------------------------------------------------------------

void Pong::onStop()
{
  // The stop button got pressed, hide the ball.
  _active = false;
  _scene->hideBall();
  updateUI();
}

//----------------------------------------------------------------------------

/**
 * @brief Called by the timer on each tick to advance the game flow.
 */
void Pong::onTick()
{
  updateRacketPositions();

  // Only if a game is active the ball is visible and we move it
  if (_active)
  {
    // Between two 'frames' the ball travels a specific distance (the distance between old and
    // new ball position) which we call 'undeflected distance'. When deflected we compute the
    // distance from the start position to the intersection point, this is the 'distance to
    // deflection'. Then we subtract the distance to deflection from the undeflected distance
    // and receive the 'rest distance'. This is the distance the ball traveled from the deflection
    // point to the new ball position.
    // For high frame rates this all may not have a bit effect, however, for low framerates it
    // should be noticable.
    auto computePosAfterDeflection = [this](QPointF newBallPos, QPointF intersectionPoint) {
      auto undeflectedLength  = QLineF{_ballPosition, newBallPos}.length();
      auto lengthToDeflection = QLineF{_ballPosition, intersectionPoint}.length();
      auto restLength         = undeflectedLength - lengthToDeflection;
      QLineF restLine{intersectionPoint, intersectionPoint + _ballVector};
      restLine.setLength(restLength);
      return restLine.p2();
    };

    // Take the current position of the ball and compute the next position as if it would not be
    // deflected.
    auto newBallPosition = _ballPosition + _ballVector * (0.5 / FPS);

    // Now move the ball and check for intersection with a wall or a racket.
    switch (auto [result, intersectionPoint] = _scene->moveBall(_ballPosition, newBallPosition);
            result)
    {
        // No Intersection, the ball moves freely around
      case PongScene::IntersectionResult::None:
        _ballPosition = newBallPosition;
        break;

      // Intersection with the left or right racket, the ball has to be deflected
      case PongScene::IntersectionResult::LeftRacket:
      case PongScene::IntersectionResult::RightRacket:
      {
        _ballVector   = QPointF{-_ballVector.x(), _ballVector.y()};
        _ballPosition = computePosAfterDeflection(newBallPosition, intersectionPoint);
        break;
      }

      // Intersection with the top or bottom border of the playing field, the ball has to be
      // defelcted
      case PongScene::IntersectionResult::Top:
      case PongScene::IntersectionResult::Bottom:
      {
        _ballVector   = QPointF{_ballVector.x(), -_ballVector.y()};
        _ballPosition = computePosAfterDeflection(newBallPosition, intersectionPoint);
        break;
      }

      // Intersection with the left border of the playing field. The ball left the playing field,
      // the player on the right scored a goal.
      case PongScene::IntersectionResult::Left:
      {
        goal(Players::P2);
        initialiseBallPosition();
        break;
      }

      // Intersection with the right border of the playing field. The ball left the playing field,
      // the player on the left scored a goal.
      case PongScene::IntersectionResult::Right:
      {
        goal(Players::P1);
        initialiseBallPosition();
        break;
      }
    }
    _scene->positionBall(_ballPosition);
  }
}

//----------------------------------------------------------------------------

void Pong::updateUI()
{
  assert(_ui->start_btn);
  assert(_ui->stop_btn);

  _ui->start_btn->setEnabled(!_active);
  _ui->stop_btn->setEnabled(_active);
  _ui->score_lbl->setText(_score.scoreStr());
}

//----------------------------------------------------------------------------

/**
 * @brief Based on whether the movement keys are pressed the player rackets are moved.
 */
void Pong::updateRacketPositions()
{
  static double step = 1.5 / FPS;

  // Update the position of the racks if a movement key has been pressed.
  if (_ui->pong_field->p1UpPressed())
  {
    _p1Position -= step;
  }
  else if (_ui->pong_field->p1DownPressed())
  {
    _p1Position += step;
  }
  _p1Position = std::max(0.0, std::min(1.0, _p1Position));

  if (_ui->pong_field->p2UpPressed())
  {
    _p2Position -= step;
  }
  else if (_ui->pong_field->p2DownPressed())
  {
    _p2Position += step;
  }
  _p2Position = std::max(0.0, std::min(1.0, _p2Position));

  _scene->positionRacket(Players::P1, _p1Position);
  _scene->positionRacket(Players::P2, _p2Position);
}

//----------------------------------------------------------------------------

void Pong::initialiseBallPosition()
{
  // Initialise the ball in the center and create a random vector.
  _ballPosition = QPointF{0.5, 0.5};

  // Only allow Angles which are 45° or less to the horizontal line as initial directions. Other
  // angles often let the ball bounce up and down lots of times before reaching the left or right
  // racket and that is boring.
  while (true)
  {
    std::random_device randomDevice;
    std::mt19937 random_engine(randomDevice());
    std::uniform_int_distribution<int> distribution(1, 360);
    auto const randomAngleDegree  = static_cast<double>(distribution(random_engine));
    auto const randomAngleRadiant = randomAngleDegree * 3.1415926535897932384626433 / 180.0;

    auto x = std::cos(randomAngleRadiant);
    auto y = std::sin(randomAngleRadiant);

    if (std::abs(x) > std::abs(y))
    {
      _ballVector = QPointF{x, y};
      return;
    }
  }
}

//----------------------------------------------------------------------------

/**
 * One of the players scored a goal. Increase its score and, if a player got three goals, end the
 * game. Otherwise just go on.
 */
void Pong::goal(Players p)
{
  switch (static_cast<Players>(p))
  {
    case Players::P1:
      ++_score.p1;
      break;

    case Players::P2:
      ++_score.p2;
      break;
  }

  updateUI();

  if (_score.hasWinner())
  {
    onStop();
    _ui->winner->setText(_score.winnerString() + " Wins !");
    _ui->winner->show();
  }
}

//----------------------------------------------------------------------------

QString Pong::Score::scoreStr() const
{
  return QString{"%1 - %2"}.arg(p1).arg(p2);
}

//----------------------------------------------------------------------------

void Pong::Score::clear()
{
  p1 = 0;
  p2 = 0;
}

//----------------------------------------------------------------------------

bool Pong::Score::hasWinner() const
{
  return ((p1 > 2) || (p2 > 2));
}

//----------------------------------------------------------------------------

QString Pong::Score::winnerString() const
{
  if (p1 > 2)
  {
    return "Player 1";
  }
  else if (p2 > 2)
  {
    return "Player 2";
  }
  return "";
}
