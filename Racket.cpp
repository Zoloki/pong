/*!
 * @file      Ball.cpp
 * @brief     A graphics item representing a Racket in the Pong game.
 * @copyright Peer Schneider
 */

#include "Racket.h"
#include <QPainter>
#include <QStyleOptionGraphicsItem>

static constexpr double cRacketWidth{10.0};
static constexpr double cRacketHeight{80.0};

Racket::Racket() {}

//----------------------------------------------------------------------------

QRectF Racket::boundingRect() const
{
  // The pen is used to draw the outline of the racket. The widht and height of the racket lays
  // exactly in the middle of the outline and so we have to consider half the pen width on each side
  // when computing the bounding rectangle because the bounding rectangle has to include the whole
  // area we are drawing into. If we would not do that we would may would see drawing artifacts.
  auto halfWidth      = cRacketWidth / 2.0;
  auto halfHeight     = cRacketHeight / 2.0;
  double penWidth     = 1;
  double halfPenWidth = penWidth / 2.0;
  return QRectF{-halfWidth - halfPenWidth, -halfHeight - halfPenWidth, cRacketWidth + penWidth,
                cRacketHeight + penWidth};
}

//----------------------------------------------------------------------------

void Racket::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  (void)option;
  (void)widget;

  painter->setBrush(Qt::black);
  auto halfWidth  = cRacketWidth / 2.0;
  auto halfHeight = cRacketHeight / 2.0;
  QRectF r{-halfWidth, -halfHeight, cRacketWidth, cRacketHeight};
  painter->drawRect(r);
}

//----------------------------------------------------------------------------

double Racket::width() const
{
  return cRacketWidth;
}

//----------------------------------------------------------------------------

double Racket::height() const
{
  return cRacketHeight;
}
