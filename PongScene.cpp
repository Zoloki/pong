/*!
 * @file      Ball.cpp
 * @brief     The scene hosting the Pong game.
 * @copyright Peer Schneider
 */

#include "PongScene.h"
#include "Racket.h"
#include "Ball.h"
#include "PongField.h"
#include <QDebug>
#include <random>

// Hardcoded playing field size. If the field is too large for your screen you have to reduce these
// values. Zooming or auto adaption is not implemented!
static constexpr QRectF cFieldRect{0.0, 0.0, 740.0, 500.0};

PongScene::PongScene(QObject* parent) : QGraphicsScene(parent), _fieldRect{cFieldRect}
{
  addRect(_fieldRect);
  setSceneRect(_fieldRect);

  _ball = new Ball{};
  _ball->hide();  // Hide initially; show only during a game
  _racketP1 = new Racket{};
  _racketP2 = new Racket{};

  addItem(_ball);
  addItem(_racketP1);
  addItem(_racketP2);
}

//----------------------------------------------------------------------------

PongScene::~PongScene()
{
  _ball     = nullptr;
  _racketP1 = nullptr;
  _racketP2 = nullptr;
}

//----------------------------------------------------------------------------

void PongScene::showBall()
{
  assert(_ball);
  _ball->show();
}

//----------------------------------------------------------------------------

void PongScene::hideBall()
{
  assert(_ball);
  _ball->hide();
}

//----------------------------------------------------------------------------

auto PongScene::moveBall(QPointF oldBallPosition, QPointF newBallPosition) const
    -> std::tuple<IntersectionResult, QPointF>
{
  auto oldBallPosPx = relativeToAbsolutePos(oldBallPosition);
  auto newBallPosPx = relativeToAbsolutePos(newBallPosition);

  QPointF intersectionPoint;

  // The line from the previous to the next, undeflected, position will be used to intersect
  // with the field borders and the rackets.
  QLineF ballLine{oldBallPosPx, newBallPosPx};

  // The field rect has to be adjusted to consider the ball diameter when intersecting with its
  // top and bottom border:
  auto ballRadius   = _ball->diameter() / 2.0;
  auto adjustedRect = _fieldRect.adjusted(ballRadius, ballRadius, -ballRadius, -ballRadius);
  QLineF topBorder{adjustedRect.topLeft(), adjustedRect.topRight()};
  QLineF bottomBorder{adjustedRect.bottomLeft(), adjustedRect.bottomRight()};

  if (topBorder.intersect(ballLine, &intersectionPoint) == QLineF::BoundedIntersection)
  {
    return std::make_tuple(IntersectionResult::Top, absoluteToRelative(intersectionPoint));
  }

  if (bottomBorder.intersect(ballLine, &intersectionPoint) == QLineF::BoundedIntersection)
  {
    return std::make_tuple(IntersectionResult::Bottom, absoluteToRelative(intersectionPoint));
  }

  // Check whether the ball would hit one of the rackets. Consider the ball radius and compute
  // virtual 'racket fronts'
  auto radius = _ball->diameter() / 2.0;
  auto p1x    = _racketP1->pos().x() + _racketP1->width() / 2.0 + radius;
  auto p2x    = _racketP2->pos().x() - _racketP2->width() / 2.0 - radius;

  auto p1top    = _racketP1->pos().y() - _racketP1->height() / 2.0 - radius;
  auto p1Bottom = _racketP1->pos().y() + _racketP1->height() / 2.0 + radius;

  auto p2top    = _racketP2->pos().y() - _racketP2->height() / 2.0 - radius;
  auto p2Bottom = _racketP2->pos().y() + _racketP2->height() / 2.0 + radius;

  QLineF p1RacketFront{QPointF{p1x, p1top}, QPointF{p1x, p1Bottom}};
  QLineF p2RacketFront{QPointF{p2x, p2top}, QPointF{p2x, p2Bottom}};

  if (p1RacketFront.intersect(ballLine, &intersectionPoint) == QLineF::BoundedIntersection)
  {
    return std::make_tuple(IntersectionResult::LeftRacket, absoluteToRelative(intersectionPoint));
  }

  if (p2RacketFront.intersect(ballLine, &intersectionPoint) == QLineF::BoundedIntersection)
  {
    return std::make_tuple(IntersectionResult::RightRacket, absoluteToRelative(intersectionPoint));
  }

  // Check whether the ball would leave the field on the left or right.
  QLineF leftBorder{_fieldRect.topLeft(), _fieldRect.bottomLeft()};
  QLineF rightBorder{_fieldRect.topRight(), _fieldRect.bottomRight()};
  if (leftBorder.intersect(ballLine, nullptr) == QLineF::BoundedIntersection)
  {
    return std::make_tuple(IntersectionResult::Left, absoluteToRelative(intersectionPoint));
  }

  if (rightBorder.intersect(ballLine, nullptr) == QLineF::BoundedIntersection)
  {
    return std::make_tuple(IntersectionResult::Right, absoluteToRelative(intersectionPoint));
  }

  // No deflection on the top, bottom, left racket or right racket and no escape no the left or
  // right.
  return std::make_tuple(IntersectionResult::None, absoluteToRelative(intersectionPoint));
}

//----------------------------------------------------------------------------

void PongScene::positionRacket(Players player, double position)
{
  double x{};
  auto margin = 5.0;

  switch (player)
  {
    case Players::P1:
      x = _fieldRect.left() + margin + _racketP1->width() / 2.0;
      break;

    case Players::P2:
      x = _fieldRect.right() - margin - _racketP1->width() / 2.0;
      break;
  }

  auto vSpace = _fieldRect.height() - margin - margin - _racketP1->height();

  // we assume all racks have the same height!
  auto y = _fieldRect.top() + margin + _racketP1->height() / 2.0 + vSpace * position;

  // The position is the center of the racket!
  auto pos = QPointF{x, y};

  switch (player)
  {
    case Players::P1:
      _racketP1->setPos(pos);
      break;

    case Players::P2:
      _racketP2->setPos(pos);
      break;
  }
}

//----------------------------------------------------------------------------

void PongScene::positionBall(QPointF pos)
{
  _ball->setPos(relativeToAbsolutePos(pos));
}

//----------------------------------------------------------------------------

QPointF PongScene::relativeToAbsolutePos(QPointF relPos) const
{
  return {relPos.x() * _fieldRect.width(), relPos.y() * _fieldRect.height()};
}

//----------------------------------------------------------------------------

QPointF PongScene::absoluteToRelative(QPointF absPos) const
{
  return {absPos.x() / _fieldRect.width(), absPos.y() / _fieldRect.height()};
}
