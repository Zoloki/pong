/*!
 * @file      Ball.cpp
 * @brief     Graphics item representing a ball in the Pong game.
 * @copyright Peer Schneider
 */

#pragma once

#include <QGraphicsItem>

class Ball : public QGraphicsItem
{
public:
  Ball();

  auto boundingRect() const -> QRectF override;
  void paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget) override;

  auto diameter() const -> double;
};
