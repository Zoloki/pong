# Pong

A very simple Pong game I created using Qt.

Player 1 (the left one) is controlled using the keys 'W' and 'S', player 2 (the right one) is controlled using the keys 'P' and 'L'.

The one scoring 3 goals first wins the match!


# Requirements

The project was developed under Kubuntu 18.04 LTS using Qt Creator 4.11.1, Qt 5.12.7 and Clang 11 using C++17 features.